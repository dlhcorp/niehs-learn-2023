---
title: "2023 NIEHS leaRn Seminar"
format: 
  revealjs:
    logo: man/dlh-logo.png
    theme: beige
    footer: "2023 NIEHS leaRn Seminar"
editor: visual
execute-dir: day1
---

## Welcome

Instructors:

-   Nat MacNell
-   Ian Buller
-   Nick Amato

GitLab Code Repository:

-   <https://gitlab.com/dlhcorp/niehs-learn-2023>

## Open-source coding with R is...

Multi-lingual:

-   R (data preparation, statistics, & data visualization)
-   markdown (documentation)
-   quarto (dynamic documentation)

Multi-platform:

-   R
-   R GUI (e.g., RStudio)
-   Web browser (e.g., RStudio server)

## Seminar Structure

-   <s>Day 1 Preparation</s>
-   Day 1 (Today): Introduction to base R
-   Day 2 Preparation
-   Day 2 (July 12th): Introduction to tidyverse R

## Orientation

1.  Open the 'niehs_2023.Rproj' file (will open RStudio)

|                            |                                 |
|----------------------------|---------------------------------|
| \[Console, Terminal, ...\] | \[Environment, History, ...\]   |
|                            | \[Files, Plots, Packages, ...\] |

2.  Navigate to /day1 subdirectory in "Files" tab
3.  Open 'day1.R' file

|             |            |
|-------------|------------|
| \[Sandbox\] | \[Memory\] |
| \[Engine\]  | \[Links\]  |

## General workflow

1.  Open, draft code, and save changes in '\*.R' files
2.  "Send" or "Submit" or "Run" code in the Console
    -   Place cursor on line of code or highlight code
    -   Click `ctrl` + `enter` (`cmd` + `enter` on Mac OSX)
    -   or click "\[-\>\] Run" button upper-right corner
    -   Or copy-paste code directly into Console right of `>`
3.  Examine output in Console

## General notation

-   Lines beginning with `#` are not "read" by R
-   Use `#` lines as comments to annotate code
-   Great code shows "how"
-   Great comments answer "why?"

Documentation is a super power

See Line 32:

```{r, echo=TRUE}
print("Hello World!")
```

## Getting started

```{r, echo=TRUE, eval=TRUE}
raw_data <- read.csv('example.csv')
```

`read.csv()` is a function

`'example.csv'` is a path to a file named 'example.csv' containing data

`<-` is the assignment operator (can sometimes use `=` instead)

`raw_data` is an object

## Viewing Data

We can view the data as a spreadsheet with a `View()` command. This will open a new tab that you can safely close.

```{r, echo=TRUE, eval=TRUE}
View(raw_data)
```

## Viewing Data

You can also print the first few rows of the dataset to the console using the name of the dataset or the `head()` function.

```{r, echo=TRUE, eval=TRUE}
raw_data
```

## Viewing Data

Alternatively, you can look at the STRucture of the dataset with `str()` This summary shows the variable name, type, and first few values of each.

```{r, echo=TRUE, eval=TRUE}
str(raw_data)
```

## Viewing Data

To inspect variable names, we can use either `colnames()` or `names()` to return a vector of variables

```{r, echo=TRUE, eval=TRUE}
colnames(raw_data)

names(raw_data)
```

## Selecting Data

Use the `$` operator after a data set name to reference a variable in that data set.

```{r, echo=TRUE, eval=TRUE}
raw_data$x
```

## Inspecting Variables

Once you have the reference, you can use R functions to get different information about the variable.

```{r, echo=TRUE, eval=TRUE}
mean(raw_data$x)
var(raw_data$x)
sd(raw_data$x)
```

## Inspecting Variables

```{r, echo=TRUE, eval=TRUE}
table(raw_data$z)
prop.table(table(raw_data$z))
```

## Inspecting Variables

You can use functions with multiple arguments, separated by commas to get more functionality.

```{r, echo=TRUE, eval=TRUE}
table(raw_data$z, raw_data$y1)
prop.table(table(raw_data$z, raw_data$y1))
```

## Inspecting Variables

By default, R uses the order of the arguments to determine which is the row and which is the column variable. Let's add named arguments to make this more clear.

```{r, echo=TRUE, eval=TRUE}
table(z = raw_data$z, y = raw_data$y1)
table(confounder = raw_data$z,
      outcome = raw_data$y1)
```

## Storing Results

You can use the assignment operator `<-` to assign (temporarily store) the results to a new object instead of printing it out to the terminal. You can then run the name of the object to see the saved output.

```{r, echo=TRUE, eval=TRUE}
tabzy <- table(confounder = raw_data$z,
               outcome = raw_data$y1)
tabzy
prop.table(tabzy)
```

## Storing Results

For longer-term storage or sharing, you can save an object into an .RData file using the `save()` function. Later, you can load the object from the file using the `load()` function.

```{r,echo=TRUE,eval=FALSE}
# save table
save(tabzy, file = file.path('day1', 'tabzy.RData'))

# remove temporary copy of table
rm(tabzy)

# load table
load('tabzy.RData')
```

## Storing Results

We can also write the output to a spreadsheet using the `write.csv()` function

```{r,eval=FALSE,echo=TRUE}
write.csv(tabzy, file = file.path('day1', 'tabzy.csv'), row.names = FALSE)

```

## Histograms

A histogram offers a simple way of checking how continuous data is distributed within a dataset. We can use a simple `hist()` function to create a histogram.

```{r, echo=TRUE,eval=TRUE}
hist(raw_data$x)
```

## Histograms

The binwidth of the histogram can easily be modified with the addition of `breaks = …` argument to the histogram function

```{r, echo=TRUE, eval=TRUE}
hist(raw_data$x, breaks = 30)
```

## Histograms

Creating a histogram of a categorical variable will simply show how many of each category are present.

```{r, echo=TRUE,eval=TRUE}
hist(raw_data$y1)
```

## Scatter Plot

A simple scatter plot can be made with the `plot()` function The first argument in the function will be the X axis, while the second argument is the Y axis.

```{r, echo=TRUE, eval=TRUE}
plot(raw_data$longitude, raw_data$latitude)
```

## Scatter Plot

If we want to change the symbol of the points to a dollar sign we can use `pch=$`. If we want to modify the color of the points, we can specify that by using `col = …`

```{r, echo=TRUE, eval=TRUE}
raw_data$y_factor <- as.factor(raw_data$y1)
plot(raw_data$longitude, raw_data$latitude, pch = "$", col = raw_data$y_factor)
```

## Box Plot

A simple boxplot can be created through the use of a the `boxplot()` command. By default, the boxplot will plot the minimum, maximum, first/second/third quartiles and outliers

```{r, echo=TRUE, eval=TRUE}
boxplot(raw_data$x ~ raw_data$y_factor)
```

## Cleaning Data

Dichotomize a variable

```{r, echo=TRUE, eval=TRUE}
# split x at 0.8
raw_data$x_bin <- raw_data$x > 500
# inspect
table(raw_data$x_bin)
```

## Cleaning Data

Categorize the data

```{r, echo=TRUE,eval=TRUE}
# split x into categories by quantile
raw_data$x_cat = cut(raw_data$x, breaks = c(0, 250, 500, 900))
# inspect
table(raw_data$x_cat)
```

## Linear Regression Model

```{r, echo=TRUE, eval=TRUE}
m_linear <- glm(y2 ~ x, data = raw_data, family = gaussian)
```

## Linear Regression Model

Inspecting our created model

```{r, echo=TRUE, eval=TRUE}
m_linear
```

## Linear Regression Model

Inspecting our created model

```{r, echo=TRUE, eval=TRUE}
summary(m_linear)
```

## Linear Regression Model

Inspecting our created model

```{r, echo=TRUE, eval=TRUE}
coef(m_linear)
coef(summary(m_linear))
confint(m_linear)
```

## Logistic Regression Model

```{r, echo=TRUE, eval=TRUE}
sample.split = sample(c(TRUE,FALSE), nrow(raw_data), replace = TRUE, prob = c(0.8,0.2))

train = raw_data[sample.split,]
test = raw_data[!sample.split,]

m_logistic <- glm(y1 ~ x, data = train, family = binomial('logit'))
```

## Logistic Regression Model

Inspecting our created model

```{r, echo=TRUE, eval=TRUE}
m_logistic
```

## Logistic Regression Model

Inspecting our created model

```{r, echo=TRUE, eval=TRUE}
summary(m_logistic)
```

## Logistic Regression Model

Inspecting our created model

```{r, echo=TRUE, eval=TRUE}
exp(coef(m_logistic))      # note that we need exponentiation to get odds ratio
coef(summary(m_logistic))
exp(confint(m_logistic))   # odds ratio
```

## Linear Regression Model Visualization

We can get a quick look at how the linear regression model was fit to our data by creating a plot.

```{r, echo=TRUE, eval=TRUE}
plot(y2 ~ x, data = raw_data)
abline(m_linear, lwd = 5)
```

## Logistic Regression Model Visualization

```{r, echo=TRUE, eval=TRUE}
test$pred = predict(m_logistic, test, type = 'response')

plot(y1 ~ x, data=test, col='steelblue')
lines(pred ~ x, test, lwd=2)
```

## Exporting Results

R has a graphics device that will render and save images

```{r, echo=TRUE, eval=FALSE}
png(filename = file.path("day1", "out_linear.png"))
plot(y2 ~ x, data = raw_data)
abline(m_linear, lwd = 5)
dev.off()
```

## Export Table Object

R can write tabular data as a tabular file type (e.g., CSV)

```{r, echo=TRUE, eval=FALSE}
write.csv(x = odd_table, file = file.path("day1", "out_logistic.csv"))
```

## Next Time

On July 12th we will return to cover these topics but instead utilizing some additional R packages to simplify and improve our work.

# Questions?

# Additional Slides

## Logistic Regression (Categorical)

```{r, echo=TRUE, eval=TRUE}
m_logistic <- glm(y1 ~ x_cat, data = raw_data, family = binomial('logit'))
logistic_ORs <- exp(coef(m_logistic))
dotchart(x = logistic_ORs)   # odds ratio (without confidence intervals)
```

## Logistic Regression (Categorical)

```{r, echo=TRUE, eval=TRUE}
logistic_CIs <- exp(confint(m_logistic))
odd_table <- data.frame(OR = logistic_ORs[2:3],
                        LC = logistic_CIs[2:3, 1],
                        UC = logistic_CIs[2:3, 2])
row.names(odd_table) <- c("x < 500", "x >= 500")
odd_table <- round(odd_table, digits = 2)
odd_table
```
