---
title: "NIEHS leaRn 2023: Day 2 Preparation"
author: |
  | Nick Amato, Nathaniel MacNell, & Ian Buller 
  | Social & Scientific Systems, Inc., a holding company of DLH Corporation
date: "2023-07-06"
output:
  rmarkdown::html_document:
    theme: readable
---
<img src="pictures/dlh-logo.png" width="120" align="right" />
<!-- 
# ------------------------------------------------------------- #
# Day 1 Preparation
#
# Created on: 2023-07-03
# Created by:
#   Nick Amato (Gitlab:@namato)
#   Nat MacNell (Gitlab:@nmacnell)
#   Ian Buller (Gitlab:@idblr)
#
# Most recently modified on: 2023-07-06
# Most recently modified by: @idblr
# ------------------------------------------------------------- #

A) The instructions for how to create a simple R Markdown file and render it into html are within this file.
B) To get the instructions in a more readable format, you need to 'knit' or render this document! 
C) To 'knit' this html document you can simply hit the 'knit' button on the top of this window or you can press CTRL+SHIFT+K (on Windows; CMD+SHIFT+K on OSX). You should have another window appear and display your knitted html!
D) A new file in your working directory should also appear named 'day2_prep.html'. Close the window that popped up when you knit your file and open the new 'day2_prep.html' file that you just created. Follow instructions from there to learn how to create your own .Rmd file!
--> 

# Creating and Running a .Rmd File

### Introduction
R Markdown files can be very useful when putting together and distributing reports or other documentation. It offers a simple way to turn your code into an easily digestible format. R Markdown allows you to incorporate code and even visualizations directly into a portable HTML document!

### Step 1
The creation of an R Markdown file is relatively straight forward. Simply go to the upper left hand corner and select the 'File' drop-down menu. From there, select 'New File' -> 'R Markdown'

![](pictures/fileRMarkdown.PNG)

Upon selecting to create a new 'R Markdown' file, you will be prompted with a screen that asks for you to title your project (we suggest you avoid overthinking as these are details that can be changed later).
<center>

![](pictures/titleRMarkdown.PNG){width="50%"}

</center>
### Step 2
You should now have a file open that looks something like this:

![](pictures/newFileRMarkdown.PNG){width="100%"}

Congrats! You've created your first R Markdown file. Now to 'knit' this file and render an .html file, you simply need to press the knit 'button' at the top of the editor or press CTRL+SHIFT+K (on Windows; CMD+SHIFT+K on OSX). Notice that again, this will create a .html file in your working directory that takes the name of your saved .Rmd file.

![](pictures/finalRMarkdown.PNG){width="100%"}

The document you just rendered has some additional information on what you can do with R Markdown. Notice how chunks of code are rendered directly into the .html file. You can also get the output of those code chunks to render! There are many additional things you can do with R Markdown, including changing the theme to offer a more appealing appearance.

### Additional Resources
* [R Markdown Cheat Sheet](https://www.rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf) by Posit, Inc.
* [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/) by Yihui Xie, J. J. Allaire, Garrett Grolemund
