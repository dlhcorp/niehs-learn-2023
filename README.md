NIEHS leaRn 2023: getting started with R <img src="day1/man/dlh-logo.png" width="120" align="right" />
===================================================

**Date repository last updated**: June 23, 2023

### Overview

Code examples and resources for getting started with R for data management, epidemiology, and public health. 

### Authors

* **Nat MacNell** - [GitLab](https://gitlab.com/nmacnell)
* **Ian Buller** - [GitLab](https://gitlab.com/idblr)
* **Nick Amato** - [GitLab](https://gitlab.com/namato)

### Getting Started

##### Day 1 Preparation

1. Download and install the latest versions of [R](https://cran.r-project.org/) and [RStudio](https://posit.co/download/rstudio-desktop/).

2. [Clone this repository](https://about.gitlab.com/handbook/business-technology/data-team/platform/rstudio/#part-3-create-an-rstudio-project-with-git) to a recognized file directory location.

3. Open RStudio.

4. Open the "day1_prep.R" file in RStudio by locating it in the "day1" subdirectory shown the bottom right panel (by default) and left clicking on its name. It will open a new upper-right panel by default. If you do not see the file, you can open the file in your "File Explorer" (Windows) or "Finder" (Mac OSX) application and double clicking. You may need to assign RStudio as the default application to open this type of file. Follow the instructions on lines 14-15.

##### Day 1

5. Open the "day1.R" file in RStudio by locating it in the "day1" subdirectory and follow the instructions.

##### Day 2 Preparation 

6. In the R console (bottom-left panel by default), run the following R script to install the `rmarkdown` package and its dependencies by copy-pasting the following line of code to the right of the ">" symbol and press the "enter" key on your keyboard. This line of R code will install functionality to work with [Rmarkdown](https://bookdown.org/yihui/rmarkdown/installation.html) files.  

```{r}
install.packages("rmarkdown", dep = TRUE)
```

7. Open the "day2_prep.Rmd" file in RStudio by locating it in the by locating it in the "day2" subdirectory and follow the instructions. This .Rmd program references the files in the "day2/pictures" folder, so make sure you are running it from a local copy of the git repository or a downloaded copy of [the project archive](https://gitlab.com/dlhcorp/niehs-learn-2023/-/archive/main/niehs-learn-2023-main.zip). 

##### Day 2

8. Open the "day2.Rmd" file in RStudio by locating it in the "day2" subdirectory and follow the instructions.

### Additional Resources

Explore suggested learning resources listed below.

#### Programming in R
* The [R project for Statistical Computing](https://www.r-project.org/)
* The [`learnr` package](https://cran.r-project.org/package=learnr)
* The [Big Book of R](https://www.bigbookofr.com/)
* ["R for Data Science"](https://r4ds.had.co.nz/index.html) by Hadley Wickham 
* The [RStudio Education - Beginners](https://education.rstudio.com/learn/beginner/)
* An [R-Ladies](https://www.rladies.org) chapter
* The [R tag](https://stackoverflow.com/questions/tagged/r) or [R Language Collective](https://stackoverflow.com/collectives/r-language) on [StackOverflow](https://stackoverflow.com/)

#### Public Health Statistical Analysis in R
* Posit community blog [R for Public Health](https://rviews.rstudio.com/2021/06/02/r-for-public-health/)
* ["Population Health Data Science with R"](https://bookdown.org/taragonmd/phds/) by Tomás J. Aragón
* ["R for Epidemiology"](https://www.r4epi.com/) by Brian Cannell and Melvin Livingston

### Acknowledgements

This repository and the materials within were created by the authors employed at Social & Scientific Systems, Inc., a holding company of DLH Corporation and funded by contract no. HHSN273201600003I between the National Institutes of Environmental Health sciences (NIEHS), National Institutes of Health (NIH), Department of Health and Human Services (DHHS) and Social & Scientific Systems, Inc.

### Questions? Feedback?

For questions about the course material, please [submit a new issue](https://gitlab.com/dlhcorp/niehs-learn-2023/issues) to the [GitLab repository](https://gitlab.com/dlhcorp/niehs-learn-2023). 
